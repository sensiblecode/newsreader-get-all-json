#!/usr/bin/env python
# encoding: utf-8
"""Get Newsreader JSON data; display on stdout.

Usage:
  newsreader_json_collect.py <url>
"""
from __future__ import unicode_literals

import codecs
import json
import logging
import sys

import docopt
import dshelpers


def main():
    #dshelpers.install_cache()
    logging.basicConfig(level=logging.INFO)
    arguments = docopt.docopt(__doc__)
    retrieve_json_data(arguments['<url>'])


def retrieve_json_data(page_url):
    done = False
    query_contents = []

    while not done:
        json_result = dshelpers.request_url(page_url).content
        json_as_dict = json.loads(json_result)
        if 'error' in json_as_dict:
            logging.info('Got error {}: exiting'.format(json_as_dict['error']))
            break
        query_contents.append(json_as_dict['payload'])

        try:
            page_url = json_as_dict['next page']
        except KeyError:
            done = True
    print query_contents
    return query_contents


if __name__ == '__main__':
    sys.stdout = codecs.getwriter('utf-8')(sys.stdout)
    main()
